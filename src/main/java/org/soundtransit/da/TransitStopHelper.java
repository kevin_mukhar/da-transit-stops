package org.soundtransit.da;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Properties;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;

public class TransitStopHelper implements DisposableBean {
    private static Logger logger = LoggerFactory
            .getLogger(TransitStopHelper.class);

    protected static final SessionFactory sessionFactory = buildSessionFactory();

    private static SessionFactory buildSessionFactory() {
        SessionFactory sf = null;
        try {
            // Use hibernate.cfg.xml to get a SessionFactory
            Configuration config = new Configuration();
            config.configure("transitstop.cfg.xml");
            Properties properties = config.getProperties();
            for (Entry<Object, Object> es : properties.entrySet()) {
                logger.trace(es.getKey() + "=" + es.getValue());
            }

            StandardServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                    .applySettings(config.getProperties()).build();

            sf = config.buildSessionFactory(serviceRegistry);
            logger.trace("session factory: {}", sf);
        } catch (Throwable ex) {
            logger.error("{}", ex.getMessage());
            logger.error("SessionFactory creation failed\n. {}", ex);
        }
        return sf;
    }

    public static List<TransitStopCount> findTopNStartTransitStops(Integer n) {
        logger.info("findTopNStartTransitStops for {}", n);

        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();

        String sql = "select originId as stopId, originName as name ,count(0) AS count from trip_itineraries where originId <> '' group by originId order by count desc limit :limit";
        SQLQuery query = session.createSQLQuery(sql).addEntity(
                TransitStopCount.class);
        query.setInteger("limit", n);

        return executeQuery(transaction, query);
    }

    /**
     * @param transaction
     * @param query
     * @return
     */
    @SuppressWarnings("unchecked")
    public static List<TransitStopCount> executeQuery(Transaction transaction,
            SQLQuery query)
    {
        List<TransitStopCount> result = new ArrayList<TransitStopCount>();
        try {
            result = (List<TransitStopCount>) query.list();
            transaction.commit();
        } catch (Throwable t) {
            logger.error("", t);
            transaction.rollback();
        }
        return result;
    }

    public static void shutdown() {
        if (sessionFactory != null) {
            logger.info("STOPPING THE HIBERNATE SESSION");
            sessionFactory.close();
        }
    }

    public static List<TransitStopCount> findTopNStartTransitStops(Integer n,
            Integer month, Integer year)
    {
        logger.info("findTopNStartTransitStops for {}. mm/yy: {}/{}", n, month,
                year);

        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();

        String sql = "select originId as stopId, originName as name ,count(0) AS count from trip_data, trip_itineraries where trip_id=id and month(requestDate)=:month and year(requestDate)=:year and originId <> '' group by originId order by count desc limit :limit";
        SQLQuery query = session.createSQLQuery(sql).addEntity(
                TransitStopCount.class);
        query.setInteger("month", month);
        query.setInteger("year", year);
        query.setInteger("limit", n);

        return executeQuery(transaction, query);
    }

    public static List<TransitStopCount> findTopNDestTransitStops(Integer n) {
        logger.info("findTopNDestTransitStops for {}", n);

        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();

        String sql = "select destId as stopId, destName as name, count(0) AS count from trip_itineraries where destId <> '' group by destId order by count desc limit :limit";
        SQLQuery query = session.createSQLQuery(sql).addEntity(
                TransitStopCount.class);
        query.setInteger("limit", n);

        return executeQuery(transaction, query);
    }

    public static List<TransitStopCount> findTopNDestTransitStops(Integer n,
            Integer month, Integer year)
    {
        logger.info("findTopNDestTransitStops for {}, mm/yy: {}/{}", n, month,
                year);

        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();

        String sql = "select destId as stopId, destName as name, count(0) AS count from trip_data, trip_itineraries where trip_id=id and month(requestDate)=:month and year(requestDate)=:year and destId <> '' group by destId order by count desc limit :limit";
        SQLQuery query = session.createSQLQuery(sql).addEntity(
                TransitStopCount.class);
        query.setInteger("month", month);
        query.setInteger("year", year);
        query.setInteger("limit", n);

        return executeQuery(transaction, query);
    }

    public static List<TransitStopCount> findTopNDestTransitStops(Integer n,
            String span, Integer amount)
    {
        logger.info("findTopNDestTransitStops; n {}, span {}, amount {}", n,
                span, amount);

        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();

        String sql = "select destId as stopId, destName as name, count(0) AS count "
                + "from trip_data, trip_itineraries "
                + "where trip_id=id and requestDate>=date_sub(now(), interval :amount "
                + span
                + ") "
                + "and destId <> '' group by destId order by count desc limit :limit";
        SQLQuery query = session.createSQLQuery(sql).addEntity(
                TransitStopCount.class);
        query.setInteger("amount", amount);
        query.setInteger("limit", n);

        return executeQuery(transaction, query);
    }

    public static List<TransitStopCount> findTopNStartTransitStops(Integer n,
            String span, Integer amount)
    {
        logger.info("findTopNStartTransitStops; n {}, span {}, amount {}", n,
                span, amount);

        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();

        String sql = "select originId as stopId, originName as name, count(0) AS count "
                + "from trip_data, trip_itineraries "
                + "where trip_id=id and requestDate>=date_sub(now(), interval :amount "
                + span
                + ") "
                + "and originId <> '' group by originId order by count desc limit :limit";
        SQLQuery query = session.createSQLQuery(sql).addEntity(
                TransitStopCount.class);
        query.setInteger("amount", amount);
        query.setInteger("limit", n);

        return executeQuery(transaction, query);
    }

    public static BigInteger findMaxTransitStopCount(String span, Integer amount)
    {
        logger.info("Entered findMaxTransitStopCount for span {}, amount {}",
                span, amount);

        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();

        String sql = "select count(originId) AS count "
                + "from trip_data, trip_itineraries "
                + "where trip_id=id and requestDate>=date_sub(now(), interval :amount "
                + span
                + ") "
                + "and originId <> '' group by originId order by count desc limit 1";
        SQLQuery query = session.createSQLQuery(sql).addScalar("count");
        query.setInteger("amount", amount);

        BigInteger bi1 = new BigInteger("0");
        try {
            bi1 = (BigInteger) query.uniqueResult();
        } catch (Throwable t) {
        }
        logger.trace("bi1: {}", bi1);

        sql = "select count(destId) AS count "
                + "from trip_data, trip_itineraries "
                + "where trip_id=id and requestDate>=date_sub(now(), interval :amount "
                + span
                + ") "
                + "and destId <> '' group by destId order by count desc limit 1";
        query = session.createSQLQuery(sql).addScalar("count");
        query.setInteger("amount", amount);

        BigInteger bi2 = new BigInteger(new byte[] { 0 });
        try {
            bi2 = (BigInteger) query.uniqueResult();
        } catch (Throwable t) {
        }
        logger.trace("bi2: {}", bi2);

        transaction.commit();
        if (bi1 == null) {
            return new BigInteger("30");
        } else {
            return (bi1.compareTo(bi2) >= 0 ? bi1 : bi2);
        }
    }

    @Override
    public void destroy() throws Exception {
        shutdown();
    }
}
