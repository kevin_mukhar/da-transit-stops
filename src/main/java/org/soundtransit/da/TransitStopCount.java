package org.soundtransit.da;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Immutable;

@Immutable
@Entity(name = "stop")
public class TransitStopCount {
    @Id
    private String stopId;

    private String name;

    private int count;

    public TransitStopCount() {
    }

    public TransitStopCount(String stopId, String name, int count) {
        this.stopId = stopId;
        this.name = name;
        this.count = count;
    }

    public String getStopId() {
        return stopId;
    }

    public String getName() {
        return name;
    }

    public int getCount() {
        return count;
    }
}
