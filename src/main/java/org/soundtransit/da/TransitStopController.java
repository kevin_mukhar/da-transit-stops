package org.soundtransit.da;

import java.math.BigInteger;
import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TransitStopController {
    @RequestMapping("/start/top/{n}/{span}/{amount}")
    public TransitStopCount[] getTopStartTransitStopsForSpan(
            @PathVariable Integer n, @PathVariable String span,
            @PathVariable Integer amount)
    {
        List<TransitStopCount> stops = TransitStopHelper
                .findTopNStartTransitStops(n, span, amount);
        return stops.toArray(new TransitStopCount[] {});
    }

    @RequestMapping("/dest/top/{n}/{span}/{amount}")
    public TransitStopCount[] getTopDestTransitStopsForSpan(
            @PathVariable Integer n, @PathVariable String span,
            @PathVariable Integer amount)
    {
        List<TransitStopCount> stops = TransitStopHelper
                .findTopNDestTransitStops(n, span, amount);
        return stops.toArray(new TransitStopCount[] {});
    }

    @RequestMapping("/max/top/{span}/{amount}")
    public int getMaxTransitStopCountForSpan(
            @PathVariable String span,
            @PathVariable Integer amount)
    {
        BigInteger count = TransitStopHelper
                .findMaxTransitStopCount(span, amount);
        return count.intValue();
    }
}
