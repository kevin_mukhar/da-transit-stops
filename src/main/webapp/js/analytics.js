$(document).ready(function() { 

  var intervalMap = {
      'minute/5': '60', // 5 minute span updates every 60 seconds
      'minute/60': '300', // 60 minute span updates every 300 seconds (5 minutes)
      'hour/24': '900', // 24 hours, 15 minute update
      'day/7': '900' // 7 days, 15 minute update
  };

  var scaleMap = {
      'minute/5': '2',
      'minute/60': '4',
      'hour/24': '5',
      'day/7': '6'
  };
  
  var interval = 0;

  function getData(topN) {
    console.log("topN=" + topN);
    var selected = $("input[type='radio'][name='time_span']:checked");
    interval = intervalMap[selected.val()] * 1000; //milliseconds

    var scale = scaleMap[selected.val()]/6;

    $.when(
        $.ajax({
          url: "max/top/" + selected.val()
        }),

        $.ajax({
          url: "start/top/" + topN + "/" + selected.val()
        }),

        $.ajax({
          url: "dest/top/" + topN + "/" + selected.val()
        })
    ).done(function(max, dataStart, dataDest) {
      var x = d3.scale.linear()
      .domain([0, max[0]])
      .range([0, scale*window.innerWidth-32]);

      $(".chart.start").empty();

      addDataToChart(".chart.start", dataStart[0], x);
      addDataToChart(".chart.dest", dataDest[0], x);
    });
    
    $(".countdown").html("Reload in " + interval/1000 + " seconds");
  }
  
  var spinner = $("#spinner").spinner({
    min: 5,
    max: 100,
    page: 25,
    step: 5,
    spin: function(event, ui) {
      $("#dest-title").html(
          "Top " + ui.value + " Destination Transit Stops");
    },
    change: function( event, ui ) {
      getData($("#spinner").spinner("value"));
    }
  });
  
  $("input[type='radio'][name='time_span']").on('change', function(){
    getData($("#spinner").spinner("value"));
  });

  function addDataToChart(name, data, x) {
    $(name).empty();
    d3.select(name)
      .selectAll("div")
      .data(data)
      .enter().append("div")
      .style("width", function(d) { return x(d.count) + "px"; })
      .style("display", "block")
      .text(function(d) { return d.count + ": " + d.name; });
  }

  function setCountDownText() {
    interval -= 1000;
    if (interval == 0) {
      $(".countdown").html("Reloading... ");
      getData($("#spinner").spinner("value"));
    } else {
      $(".countdown").html("Reload in " + interval/1000 + " seconds");
    }
  }

  interval = 1000;
  setInterval(setCountDownText, 1000);
});
